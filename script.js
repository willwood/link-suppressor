// Link Suppressor script by Will Woodgate - visit http://www.willwoodgate.com/ for more information
$(document).ready(function(){
	$("#nav li:has(ul)").hover(function() {
		$(this).children("a").addClass("nolink").click(function(e) {
			e.preventDefault();
		});
	});
});